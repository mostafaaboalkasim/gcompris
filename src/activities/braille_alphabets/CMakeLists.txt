#=============================================================================
# SPDX-FileCopyrightText: 2015 Arkit Vora <arkitvora123@gmail.com>
#
# SPDX-License-Identifier: BSD-2-Clause
#=============================================================================
GCOMPRIS_ADD_RCC(activities/braille_alphabets
  QML_FILES
  *.qml
  *.js
  RESOURCES
  *.svg
  resource/*.svg
)
