#=============================================================================
# SPDX-FileCopyrightText: 2015 Amit Tomar <a.tomar@outlook.com>
#
# SPDX-License-Identifier: BSD-2-Clause
#=============================================================================
GCOMPRIS_ADD_RCC(activities/missing-letter
  QML_FILES
  *.qml
  *.js
  RESOURCES
  *.svg
  resource/*.svg
)
